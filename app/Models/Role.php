<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    public $tableName;

    public function __construct(){
        $this->tableName = 'roles';
    }
    public function users(){
	    return $this->hasMany(User::class);
    }
}
