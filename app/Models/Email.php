<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    public $emailValidationRules = [
        'senderName' => 'string|required',
        'senderEmail' => 'email:rfc,dns|required',
        'emailSubject' => 'string|required',
        'message' => 'string|required'
    ];
    
    
}
