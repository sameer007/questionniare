<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Options extends Model
{
    use HasFactory;

    public $tableName;

    private function __construct(){
        $this->tableName = 'options';
    }
}
