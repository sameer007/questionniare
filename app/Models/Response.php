<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Response extends Model
{
    use HasFactory;
    public $tableName;

    public function __construct(){
        $this->tableName = 'responses';
    }

    public function submitStudentResponse($dataToinsert){
        $isInserted = DB::table($this->tableName)->insert($dataToinsert);
        if ($isInserted) {
            $insertedResponseId = DB::getPdo()->lastInsertId();
            return $insertedResponseId;
        } else {
            return $isInserted;
        }
    }
    
    
}
