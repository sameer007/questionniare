<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $tableName;

    public function __construct(){
        $this->tableName = 'users';
    }

    public $getValidationRules = [
        'email' => 'email|required',
        'password' => 'required',

    ];

    public function getAllUsers(){
        $users = DB::table($this->tableName)
            ->get();
        return $users;
    }

    public function getUserByEmail($email){
        $user = DB::table($this->tableName)
            ->where('email', $email)
            ->get();
        return $user;
    }

    public function getUserByRole($role){
        $user = DB::table($this->tableName)
            ->select('users.id','users.role_id','roles.role_name','roles.role_name','users.name','users.email','users.remember_token','users.created_at','users.updated_at')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->where('roles.role_name', $role)
            ->get();
        return $user;
    }

    public function getUserByRoleIdAndToken($role,$id,$remember_token){
        $user = DB::table($this->tableName)
            ->select('users.id','users.role_id','roles.role_name','roles.role_name','users.name','users.email','users.remember_token','users.created_at','users.updated_at')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->where('roles.role_name', $role)
            ->where('users.id', $id)
            ->where('users.remember_token', $remember_token)
            ->get();
        return $user;
    }

    public function getAllUserDataByEmail($email,$is_die = false){
        $users = DB::table($this->tableName)
            ->where('users.email', $email)
            ->get();
        
        if($is_die){
            dd(DB::getQueryLog());
        }
        return $users;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsTo(Role::class);
    }
}
