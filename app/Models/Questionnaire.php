<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Hamcrest\Type\IsInteger;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Questionnaire extends Model
{
    use HasFactory;
    public $tableName;

    public function __construct(){
        $this->tableName = 'questionnaires';
    }
    public $generateQuestionnaireValidationRules = [
        'title' => 'string|required',
        'expiryDate' => 'date|required'
    ];
    public function getAllUnexpiredQuestionnaire($is_die = false)
    {
        $questionnaires = DB::table($this->tableName)
            ->where('expiry_date', '>', now())
            ->get();
        //dd($questionnaires);
        if ($is_die) {
            dd(DB::getQueryLog());
        }
        return $questionnaires;
    }

    public function createNewQuestionnaire($dataToinsert)
    {
        //dd($dataToinsert);
        $isInserted = DB::table($this->tableName)->insert($dataToinsert);
        if ($isInserted) {
            $insertedQuestionnaireId = DB::getPdo()->lastInsertId();
            return $insertedQuestionnaireId;
        } else {
            return $isInserted;
        }
    }

    public function getQuestionnaireById($questionnaireId, $is_die = false)
    {
        $users = DB::table($this->tableName)
            ->where('id', $questionnaireId)
            ->get();

        if ($is_die) {
            dd(DB::getQueryLog());
        }
        return $users;
    }

    public function getQuestionAndOptionsByQuestionnaireId($questionnaireId = 1){
        $allQuestionsAndOptions = DB::table($this->tableName)
            ->select('questionnaires.id','questions.id AS question_id','options.id AS option_id','questionnaires.title','questions.subject','questions.question','options.option','questionnaires.created_at','questionnaires.updated_at')
            ->join('questions', 'questions.questionnaire_id', '=', 'questionnaires.id')
            ->join('options', 'options.question_id', '=', 'questions.id')
            ->where('questions.questionnaire_id', $questionnaireId)
            ->where('questionnaires.expiry_date', '>', now())
            ->get();

            dd($allQuestionsAndOptions);

        $arrangedQuestionnaireData = array();
        $options = array();
        $optionItems = array() ;
        foreach ($allQuestionsAndOptions as $key => $items) {
            
            $varTocheckInteger = ($key + 1)/4;

            //array_push($optionItems,$items->option_id);
            array_push($optionItems,$items->option.'-'.$items->option_id);
            //array_push($optionItems,$items->question_id);

            array_push($options,$optionItems);
            //$arrangedQuestionnaireData[$items->title][$items->question]['question_id'] = $items->question_id ;
            $arrangedQuestionnaireData[$items->title][$items->question.'-'.$items->question_id]['options'] = $optionItems ;
            //reset array in gap of 4 loop for options
            if(is_int($varTocheckInteger)){
                $options = array();
                $optionItems = array();
            }
            
        }
        //dd($arrangedQuestionnaireData);

        return $arrangedQuestionnaireData;
    }

}
