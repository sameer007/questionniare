<?php

namespace App\Http\Controllers;

use App\Models\Questionnaire;
use App\Models\Response;
use App\Models\User;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    /**
     * Display form to be filled by student
     * 
     * */
    public function showStudentResponseForm(Request $request, $studentId, $questionnaireId, $rememberToken)
    {
        $questionnaire = new Questionnaire();
        $user = new User();
        //dd($studentId);

        $studentData = $user->getUserByRoleIdAndToken('Student', $studentId, $rememberToken);

        if (isset($studentData) && !empty($studentData)) {
            $allQuestionsAndAnswers = $questionnaire->getQuestionAndOptionsByQuestionnaireId($questionnaireId);
            dd($allQuestionsAndAnswers);
            $resData = array(
                'allQuestionsAndAnswers' => $allQuestionsAndAnswers,
                'questionnaireId' => $questionnaireId,
                'studentId' => $studentId
            );

            return view('response', $resData);
        } else {
            return redirect('auth/login')->with([
                'error' => 'Sorry! Your record was not found'
            ]);
        }
    }
    /**
     * Store student's answers of Questionnaire in database
     * 
     * */
    public function submitStudentResponse(Request $request, $studentId, $questionnaireId)
    {
        $questionnaire = new Questionnaire();
        $response = new Response();

        $allQuestionsAndAnswers = $questionnaire->getQuestionAndOptionsByQuestionnaireId($questionnaireId);

        foreach ($allQuestionsAndAnswers as $questionaireTitle => $questionsArr) {
            foreach ($questionsArr as $questionWithId => $optionsArr['options']) {
                $question = explode('-', $questionWithId)[0];
                $questionID = explode('-', $questionWithId)[1];

                $nameValue = 'option-' . $questionID;


                foreach ($optionsArr['options'] as $optionsArrkey => $optionItems) {
                    foreach ($optionItems as $key => $items) {
                        $nameAttr = explode('-', $items)[1];
                        if (!isset($nameAttr) || empty($nameAttr)) {
                            return view('index')->with([
                                'error' => 'Invalid Data detected',
                            ]);
                        }
                    }
                }
            }
        }
        //dd($allQuestionsAndAnswers);
        //dd($request->all());
        $dataToInsertList = array();
        foreach ($request->all() as $key => $items) {
            if (isset(explode('-', $items)[1]) && isset(explode('-', $key)[1])) {
                $questionId = explode('-', $key)[1];

                $optionId = explode('-', $items)[1];
                //dd($key);
                $dataToinsert = array(
                    'questionnaire_id' => $questionnaireId,
                    'question_id' => $questionId,
                    'user_id' => $studentId,
                    'selected_option_id' => $optionId
                );

                array_push($dataToInsertList, $dataToinsert);
            }
        }
        //dd($dataToInsertList);

        $response_id = $response->submitStudentResponse($dataToInsertList);

        //dd($response_id);
        if ($response_id) {
            return redirect('auth/login')->with([
                'success' => 'Answers Submitted Successfully',
            ]);
        } else {
            return redirect('auth/login')->with([
                'error' => 'Something went wrong while submitting answers',
            ]);
        }
    }
}
