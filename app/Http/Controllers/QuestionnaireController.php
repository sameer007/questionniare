<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\Questionnaire;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class QuestionnaireController extends Controller
{
    /**
     * Display add Questionnare view
     * 
     * */
    public function showGenerateQuestionnaire(Request $request)
    {
        //dd($request->session());

        return view('questionnaire/addQuestionnaire');
    }
    /**
     * Display list of Questionnaire
     * 
     * */
    public function showQuestionnaireList(Request $request)
    {
        $questionnaire = new Questionnaire();
        $allQuestionnaire = $questionnaire->getAllUnexpiredQuestionnaire();
        //dd(csrf_token());
        if (isset($allQuestionnaire)) {
            $resData = array('allQuestionnaire' => $allQuestionnaire);
            return view('questionnaire/questionnaireList', $resData);
        } else {
            return redirect('home/dashboard')->with([
                'error' => 'Sorry! Questionnaire list was not found'
            ]);
        }
    }
    
    /**
     * Generage questionnaire for students and invitation emails for all students
     * 
     * */
    public function generateQuestionnaire(Request $request)
    {


        $questionnaire = new Questionnaire();

        $validator = Validator::make($request->all(), $questionnaire->generateQuestionnaireValidationRules);

        if ($validator->fails()) {
            dd(gettype($request->expiryDate));
            return back()->withErrors($validator);
        } else {

            $dataToinsert = array(
                'title' => $request->title,
                'expiry_date' => $request->expiryDate

            );

            $questionnaire_id = $questionnaire->createNewQuestionnaire($dataToinsert);


            if ($questionnaire_id) {

                //dd($book_id);
                $createdQuestionnaire = $questionnaire->getQuestionnaireById($questionnaire_id);
                //dd($createdQuestionnaire);

                $user = new User();
                $allStudents = $user->getUserByRole('Student');
                //dd($allStudents);

                foreach ($allStudents as $key => $items) {

                    $subject = 'Invitation to Participate in a Questionnaire titled : `' . $createdQuestionnaire[0]->title;
                    // Send unique emails to multiple recipients

                    $mailData = array(
                        'studentId' => $items->id,
                        'rememberToken' => $items->remember_token,
                        'studentName' => $items->name,
                        'questionnaireId' => 1, // 1 is set by default because default data with id is seeded
                        'questionnaireTitle' => $createdQuestionnaire[0]->title,
                        'questionnaireExpiryDate' => date("Y-m-d", strtotime($createdQuestionnaire[0]->expiry_date)) . ' 12:00 PM'
                    );
                    $user = array(
                        'to' => $items->email,
                        'subject' => $subject
                    );
                    Mail::send('emailTemplate', $mailData, function ($messages) use ($user) {
                        $messages->to($user['to']);
                        $messages->subject($user['subject']);
                    });
                }

                if (count(Mail::failures()) > 0) {

                    $errors = 'Failed to send password reset email, please try again.';

                    return redirect()->route('questionnaireList')->with([
                        'error' => 'Failed to send emails to students, please try again.'
                    ]);
                } else {

                    return redirect()->route('questionnaireList')->with([
                        'success' => 'Questionnaire created with email invitation successfully',
                    ]);
                }
            } else {
                return view('questionnaire/addQuestionnaire')->with([
                    'error' => 'Something went wrong while adding Questionnaire info',
                ]);
            }
        }
    }
}
