<?php

namespace App\Http\Controllers;

use App\Models\Email;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    /**
     * Display email view
     * 
     * */
    public function showEmail(Request $request)
    {
        //dd($request->session());

        return view('email');
    }
    /**
     * Send email to users
     * 
     * */
    public function sendEmail(Request $request)
    {

        $email = new Email();
        $validator = Validator::make($request->all(), $email->emailValidationRules);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            $mailData = array(
                'name' => $request->senderName,
                'data' => $request->message,
            );
            $user = array(
                'to' => 'sam.codename007@gmail.com',
                'subject' => $request->emailSubject
            );

            $files = array();
            $attachmentFilePathArr = array();
            foreach ($request->file('attachmentFiles') as $key => $filePropItems) {
                $customFileName = 'file-' . now() . '.' . $filePropItems->getClientOriginalExtension();
                //$filePropItems->storeAs('public/files/email', $customFileName);
                $path = $filePropItems->store('public/files/email');
                //dd($path);

                $uploadedFileName = basename($path);

                $attachmentFilePath = storage_path('app/public/files/email/' . $uploadedFileName);
                array_push($files, $attachmentFilePath);
            }

            Mail::send('emailTemplate', $mailData, function ($messages) use ($user, $files) {
                $messages->to($user['to']);
                $messages->subject($user['subject']);


                foreach ($files as $file) {
                    $messages->attach($file);
                }
            });

            if (count(Mail::failures()) > 0) {

                $errors = 'Failed to send password reset email, please try again.';

                return redirect('home/email')->with([
                    'error' => 'Failed to send email, please try again.'
                ]);
            } else {

                return redirect('home/email')->with([
                    'success' => 'Mail sent successfully.'
                ]);
            }
        }
    }
}
