<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display login view
     * 
     * */
    public function login()
    {
        return redirect('home/dashboard');
    }
    /**
     * Logic to handle user login or display index page
     * 
     * */
    public function showLoginPage(Request $request)
    {
        if (Auth::check()) {
            // The user is logged in...
            $loggedinUserData = $request->session()->get('loggedinUserData');
            if (isset($loggedinUserData) && !empty($loggedinUserData)) {
                return redirect('home/dashboard/')->with([
                    'success' => 'Logged in successfully as ' . $loggedinUserData->name,
                ]);
            } else {
                return redirect('auth/logout/');
            }
        } else {
            return view('index');
        }
    }
    /**
     * Logic to handle user authentication
     * 
     * */
    public function authenticate(Request $request)
    {
        $user = new User();
        $validator = Validator::make($request->all(), $user->getValidationRules);

        $hashedPassword = Hash::make($request->password);

        $isPasswordCorrect = Hash::check($request->password, $hashedPassword);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            if ($isPasswordCorrect) {
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    $request->session()->regenerateToken();
                    $user = new User();
                    $loggedinUserData = $user->getAllUserDataByEmail($request->email);
                    //dd($loggedinUserData[0]);

                    if (isset($loggedinUserData[0]) && !empty($loggedinUserData[0])) {
                        $request->session()->put('loggedinUserData', $loggedinUserData[0]);

                        return redirect('home/dashboard/')->with([
                            'success' => 'Logged in successfully as ' . $loggedinUserData[0]->name,
                        ]);
                    } else {
                        return back()->with([
                            'error' => 'Something went wrong while retreiving logged in user data',
                        ]);
                    }
                }

                return back()->with([
                    'error' => 'The provided credentials do not match our records.',
                ]);
            } else {
                //dd('hash value incorrect', $hashedPassword);
                return back()->with([
                    'error' => 'Provided email or password do not match our records.',
                ]);
            }
        }
    }
    /**
     * Log user out of system
     * 
     * */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('auth/login')->with([
            'success' => 'Logged out successfully',
        ]);
    }
    /**
     * Register new user
     * 
     * */
    public function register(Request $request)
    {

        $user = new User();

        $validator = Validator::make($request->all(), $user->getValidationRules);
        $allUsers =  $user->getAllUsers();

        dd($allUsers);

        if ($validator->fails()) {
            return redirect('auth/login')->withErrors($validator);
        } else {
            return redirect('auth/login');
        }
    }
    /**
     * Display registration view
     * 
     * */
    public function showRegisterPage()
    {
        return view('register');
    }
}
