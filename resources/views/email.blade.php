@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Email</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> Edit Book info</h4>
                        <form id="addBookForm" action="./sendEmail" enctype="multipart/form-data" METHOD="POST">
                            @csrf()
                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Your Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" type="text" placeholder="Sender Name" name="senderName" required="" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Your Email</label>
                                <small class="req"> *</small>
                                <input class="form-control" type="email" placeholder="email@emailServiceProvider.com" name="senderEmail" required="" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <label>Subject</label>
                                <input class="form-control" type="text" placeholder="Email subject" name="emailSubject" required="" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Attachment File/s</label>
                                <small class="req"> *</small>
                                <input class="form-control" type="file" name="attachmentFiles[]" multiple="multiple" /><br />
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <label>Message Text</label>
                                <textarea class="form-control" name="message" id="contact-message" placeholder="Your Message" rows="5" data-rule="required" data-msg="Please write something for us"></textarea>
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="send-email">Send Email</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')

    @endsection