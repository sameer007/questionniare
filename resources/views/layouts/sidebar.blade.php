<aside>
@php

$currentPage = substr(strrchr(url()->current(),"/"),1);

$loggedinUserData = session()->get('loggedinUserData');

if(isset($loggedinUserData) && !empty($loggedinUserData)){
    
    if(isset($loggedinUserData->image) && !empty($loggedinUserData->image)){
        $profileUrl = asset('storage/images/profile/'.$loggedinUserData->image);

    }else{
        $profileUrl = url('img/ui-sam.jpg');

    }
}else{
}
@endphp

        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <p class="centered"><a href="profile.html"><img src="{{$profileUrl}}" class="img-circle" width="80"></a></p>
                <h5 class="centered">{{(isset($loggedinUserData->name) && !empty($loggedinUserData->name)) ? $loggedinUserData->name : '' }}</h5>
                
                <li class="mt">
                    <a class = "{{ ($currentPage == 'dashboard') ? 'active' : ''  }}" href="{{url('home/dashboard')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="mt">
                    <a class = "{{ ($currentPage == 'addQuestionnaire') ? 'active' : ''  }}" href="{{url('home/questionnaire/generate')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Add Questionnaire</span>
                    </a>
                </li>
                <li class="mt">
                    <a class = "{{ ($currentPage == 'list') ? 'active' : ''  }}" href="{{url('home/questionnaire/list')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>View Questionnaire</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>