<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashio - Bootstrap Admin Template</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="{{url('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  
  <!--external css-->
  <link href="{{url('lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
  <link href="{{url('lib/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet"/>
  <link href="{{url('lib/bootstrap-datepicker/css/datepicker.css')}}" rel="stylesheet"/>
  <link href="{{url('lib/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet"/>
  <link href="{{url('lib/bootstrap-timepicker/compiled/timepicker.css')}}" rel="stylesheet"/>
  <link href="{{url('lib/bootstrap-datetimepicker/datertimepicker.css')}}" rel="stylesheet"/>

  <!-- Custom styles for this template -->
  <link href="{{url('css/style.css')}}" rel="stylesheet">
  <link href="{{url('css/style-responsive.css')}}" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>