@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Book</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                    <table class="table table-striped table-advance table-hover">
                        <h4><i class="fa fa-angle-right"></i> Unexpired Questionnaire List <a class="btn btn-primary pull-right" href="{{url('home/questionnaire/generate')}}">Add Questionnaire </a></h4>
                        <hr />
                        @php
                        //debugger($allQuestionnaire);
                        @endphp
                        <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Title</th>
                                <th>Expiry Date</th>
                                <th>Added Date</th>
                                <th>Updated Date</th>
                            </tr>
                        </thead>
                        <tbody id="bookListTbody">
                            @foreach ($allQuestionnaire as $items)
                            
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{$items->title}}</td>
                                <td>{{$items->expiry_date}}</td>
                                <td>{{$items->created_at}}</td>
                                <td>{{$items->updated_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')

    @endsection