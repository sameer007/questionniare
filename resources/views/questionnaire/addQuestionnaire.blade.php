@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Book</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> Add Questionnaire</h4>
                        <form id="addQuestionnaireForm" action="./generate" METHOD="POST">
                            @csrf()
                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Title</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="title" autofocus="" name="title" placeholder="" type="text" value="" autocomplete="off" required="true" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Expiry Date</label>
                                <input class="form-control form-control-inline input-medium" size="16" type="date" name="expiryDate">
                                    
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit">Generate</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->


    @include('layouts.footer')
    <!--script for this page-->
    <script src="{{url('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
    <script src="{{url('lib/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
    <script src="{{url('lib/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('lib/bootstrap-daterangepicker/date.js')}}"></script>
    <script src="{{url('lib/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{url('lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{url('lib/bootstrap-daterangepicker/moment.min.js')}}"></script>
    <script src="{{url('lib/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
    <script src="{{url('lib/advanced-form-components.js')}}"></script>

    @endsection