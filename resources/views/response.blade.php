@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->


    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                    
                        
                        <form class=" bg-white px-4" id="addQuestionnaireForm" action="{{url('test/response/')}}/{{$studentId}}/{{$questionnaireId}}" METHOD="POST">
                            @csrf()

                            @foreach ($allQuestionsAndAnswers as $questionaireTitle => $questionsArr)
                            <h4 class="mb"><i class="fa fa-angle-right"></i> {{$questionaireTitle}}</h4>

                            @php

                            $i = 0;

                            @endphp
                            @foreach ($questionsArr as $questionWithId => $optionsArr['options'])
                            @php

                            $i++;
                            $question = explode('-',$questionWithId)[0];
                            $questionID = explode('-',$questionWithId)[1];

                            @endphp
                            <p>{{$i.'. '.$question}}</p>


                            @foreach ($optionsArr['options'] as $optionsArrkey => $optionItems)


                            @foreach ($optionItems as $key => $items)
                            
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="option-{{$questionID}}" value="{{$items}}" />
                                <label class="form-check-label" for="radioExample1">
                                    {{explode('-',$items)[0]}}
                                </label>
                            </div>


                            @endforeach
                            <br />
                            @endforeach
                            @endforeach
                            @endforeach

                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit">Submit</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')

    @endsection