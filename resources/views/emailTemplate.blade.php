@extends('layouts.app')

@section('content')
<section id="container">


    <section class="panel">
        <div class="panel-body ">
            <div class="view-mail">
                <p>Dear {{$studentName}},</p>
                <br/>
                <p>We hope this email finds you well and thriving in your academic journey. We are excited to invite you to an engaging and knowledge-enhancing MCQ (Multiple Choice Question) test, designed to challenge your intellect and assess your understanding of physics and chemistry subjects.</p>

                <p>This test needs to be completed before : {{$questionnaireExpiryDate}}</p>

                <p>To participate in the test, simply click on the button below</p>
                <br/>
                <p>Best regards,</p>
                <p>QuestionnaireSysPvtLtd</p>

            </div>
            <div class="compose-btn pull-left">
                <a href="{{url('test/response/')}}/{{$studentId}}/{{$questionnaireId}}/{{$rememberToken}}" class="btn btn-sm btn-theme"><i class="fa fa-reply"></i> Take Questionnaire</a>
            </div>
        </div>
    </section>
</section>


@endsection