@extends('layouts.app')

@section('content')
<div id="login-page">
  <div class="container">
  
    <form class="form-login" action="/auth/login" method="post">
      @csrf()
      
      <h2 class="form-login-heading">Login now</h2>
      @include('layouts.notify')
      <div class="login-wrap">
        <input type="email" name="email" class="form-control" placeholder="User Email">
        <br>
        <input type="password" name="password" class="form-control" placeholder="Password">
        <label class="checkbox">
          <input type="checkbox" name="rememberMe" value="remember-me"> Remember me
          <span class="pull-right">
            <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
          </span>
        </label>
        <button class="btn btn-theme btn-block" href="./register" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
        <hr>
        <div class="login-social-link centered">
          <p>or you can sign in via your social network</p>
          <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
          <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
        </div>
        <div class="registration">
          Don't have an account yet?<br />
          <a class="" href="{{url('./auth/register')}}">
            Create an account
          </a>
        </div>
      </div>
      <!-- Modal -->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Forgot Password ?</h4>
            </div>
            <div class="modal-body">
              <p>Enter your e-mail address below to reset your password.</p>
              <input type="text" name="resetPwdEmail" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
              <button class="btn btn-theme" type="button">Submit</button>
            </div>
          </div>
        </div>
      </div>
      <!-- modal -->
    </form>
  </div>
</div>
@endsection
@section('customJs')
<script type="text/javascript" src="{{url('lib/jquery.backstretch.min.js')}}"></script>
<script>
  $.backstretch("{{url('img/login-bg.jpg')}}", {
    speed: 500
  });
</script>
@endsection