<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\QuestionnaireController;
use App\Http\Controllers\ResponseController;
use App\Http\Controllers\UserController;

use App\Models\Questionnaire;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('home/dashboard');
        
    }else{
        return redirect('auth/login');

    }
});
Route::group(['prefix' => 'auth'],function () {

    Route::post('/login', [UserController::class,'authenticate']);
    
    Route::post('/register', [UserController::class,'register']);

    Route::get('/login', [UserController::class,'showLoginPage'])->name('login');
    Route::get('/logout', [UserController::class,'logout'])->name('logout');


});

Route::group(['prefix' => 'home',  'middleware' => 'auth'],function () {

    Route::group(['prefix' => 'questionnaire'],function () {

        Route::get('/generate', [QuestionnaireController::class,'showGenerateQuestionnaire'])->name('showGenerateQuestionnaire');
        Route::post('/generate', [QuestionnaireController::class,'generateQuestionnaire']);

        Route::get('/list', [QuestionnaireController::class,'showQuestionnaireList'])->name('questionnaireList');
    
    });
    
    Route::get('/dashboard', [DashboardController::class,'showDashboard'])->name('showDashboard');
    
});
Route::group(['prefix' => 'test'],function () {

    Route::group(['prefix' => 'response'],function () {

        Route::get('/{studentID}/{questionnaireId}/{rememberToken}', [ResponseController::class,'showStudentResponseForm'])->name('showStudentResponseForm');
        Route::post('/{studentID}/{questionnaireId}/{rememberToken}', [ResponseController::class,'submitStudentResponse'])->name('submitStudentResponse');

    });
    
    Route::get('/dashboard', [DashboardController::class,'showDashboard'])->name('showDashboard');
    
});

