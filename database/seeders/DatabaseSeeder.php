<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(10)->create();

        /**
         * Seed roles table
         *
         */
        if (!DB::table('roles')->exists()) {
            DB::table('roles')->insert(
                [
                    [
                        'role_name' => 'Admin',
                        'role_slug' => 'admin',
                    ],
                    [
                        'role_name' => 'Student',
                        'role_slug' => 'student',
                    ]
                ]
            );
        }
        /**
         * Seed users table
         *
         */
        if (!DB::table('users')->exists()) {
            DB::table('users')->insert(
                [
                    [
                        'name' => 'Admin',
                        'email' => 'admin@questionare.com',
                        'role_id' => 1,
                        'email_verified_at' => now(),
                        'remember_token' => Str::random(60),
                        'password' => bcrypt('admin')
                    ],
                    [
                        'name' => 'Sameer',
                        'email' => 'sameeracharya_007@yahoo.com',
                        'role_id' => 2,
                        'email_verified_at' => now(),
                        'remember_token' => Str::random(60),
                        'password' => bcrypt('sameeracharya_007')
                    ]
                ]
            );
        }
        /**
         * Seed questionnaires table
         *
         */
        if (!DB::table('questionnaires')->exists()) {
            DB::table('questionnaires')->insert(
                [
                    [
                        'title' => 'Questionnaire Set A',
                        'expiry_date' => now()->addDays(7),
                    ],
                    [
                        'title' => 'Questionnaire Set B',
                        'expiry_date' => now()->addDays(10),
                    ]
                ]
            );
        }
        /**
         * Seed questions table
         *
         */
        if (!DB::table('questions')->exists()) {
            DB::table('questions')->insert(
                [
                    ['questionnaire_id' => 1, 'subject' => 'Physics', 'question' => 'What is the SI unit of force?'],
                    ['questionnaire_id' => 1, 'subject' => 'Physics', 'question' => 'Which law of motion states that every action has an equal and opposite reaction?'],
                    ['questionnaire_id' => 1, 'subject' => 'Physics', 'question' => 'What type of energy is stored in a stretched rubber band?'],
                    ['questionnaire_id' => 1, 'subject' => 'Physics', 'question' => 'The process of converting a solid directly into a gas without passing through the liquid state is called?'],
                    ['questionnaire_id' => 1, 'subject' => 'Physics', 'question' => 'Which of the following electromagnetic waves has the shortest wavelength?'],
                    ['questionnaire_id' => 1, 'subject' => 'Chemistry', 'question' => 'Which of the following elements is a noble gas and is used in balloons to make them float?'],
                    ['questionnaire_id' => 1, 'subject' => 'Chemistry', 'question' => 'What is the chemical formula for water?'],
                    ['questionnaire_id' => 1, 'subject' => 'Chemistry', 'question' => 'Which gas is released when an acid reacts with a metal?'],
                    ['questionnaire_id' => 1, 'subject' => 'Chemistry', 'question' => "What is the main component of the Earth's atmosphere?"],
                    ['questionnaire_id' => 1, 'subject' => 'Chemistry', 'question' => 'Which of the following elements is a metal and is essential for building bones and teeth in humans?'],
                
                    ['questionnaire_id' => 2, 'subject' => 'Physics', 'question' => 'Which type of electromagnetic radiation has the highest energy?'],
                    ['questionnaire_id' => 2, 'subject' => 'Physics', 'question' => 'What is the momentum of an object with a mass of 5 kg and a velocity of 3 m/s?'],
                    ['questionnaire_id' => 2, 'subject' => 'Physics', 'question' => 'Explain the concept of terminal velocity in the context of falling objects.'],
                    ['questionnaire_id' => 2, 'subject' => 'Physics', 'question' => 'What is the relationship between frequency, wavelength, and the speed of a wave?'],
                    ['questionnaire_id' => 2, 'subject' => 'Physics', 'question' => 'What is the first law of thermodynamics also known as?'],
                    ['questionnaire_id' => 2, 'subject' => 'Chemistry', 'question' => 'What is the process of a substance changing directly from a gas to a solid called?'],
                    ['questionnaire_id' => 2, 'subject' => 'Chemistry', 'question' => 'Which gas is responsible for the characteristic smell of rotten eggs?'],
                    ['questionnaire_id' => 2, 'subject' => 'Chemistry', 'question' => 'What is the term used for a substance that speeds up a chemical reaction without being consumed in the reaction?'],
                    ['questionnaire_id' => 2, 'subject' => 'Chemistry', 'question' => 'Which element, when burned, produces a green flame?'],
                    ['questionnaire_id' => 2, 'subject' => 'Chemistry', 'question' => 'What is the chemical symbol for sodium?']
                ]
            );
        }
        /**
         * Seed options table
         *
         */
        if (!DB::table('options')->exists()) {
            DB::table('options')->insert(
                [
                    ["question_id" => 1, "option" => "Newton", "is_correct" => true],
                    ["question_id" => 1, "option" => "Kilogram", "is_correct" => false],
                    ["question_id" => 1, "option" => "Meter", "is_correct" => false],
                    ["question_id" => 1, "option" => "Joule", "is_correct" => false],
                    ["question_id" => 2, "option" => "Newton's First Law", "is_correct" => false],
                    ["question_id" => 2, "option" => "Newton's Second Law", "is_correct" => false],
                    ["question_id" => 2, "option" => "Newton's Third Law", "is_correct" => true],
                    ["question_id" => 2, "option" => "Newton's Law of Gravitation", "is_correct" => false],
                    ["question_id" => 3, "option" => "Kinetic Energy", "is_correct" => false],
                    ["question_id" => 3, "option" => "Thermal Energy", "is_correct" => false],
                    ["question_id" => 3, "option" => "Potential Energy", "is_correct" => true],
                    ["question_id" => 3, "option" => "Chemical Energy", "is_correct" => false],
                    ["question_id" => 4, "option" => "Evaporation", "is_correct" => false],
                    ["question_id" => 4, "option" => "Condensation", "is_correct" => false],
                    ["question_id" => 4, "option" => "Sublimation", "is_correct" => true],
                    ["question_id" => 4, "option" => "Vaporization", "is_correct" => false],
                    ["question_id" => 5, "option" => "Radio Waves", "is_correct" => false],
                    ["question_id" => 5, "option" => "Microwaves", "is_correct" => false],
                    ["question_id" => 5, "option" => "Visible Light", "is_correct" => false],
                    ["question_id" => 5, "option" => "Gamma Rays", "is_correct" => true],
                    ["question_id" => 6, "option" => "Helium", "is_correct" => true],
                    ["question_id" => 6, "option" => "Oxygen", "is_correct" => false],
                    ["question_id" => 6, "option" => "Hydrogen", "is_correct" => false],
                    ["question_id" => 6, "option" => "Nitrogen", "is_correct" => false],
                    ["question_id" => 7, "option" => "H2O2", "is_correct" => false],
                    ["question_id" => 7, "option" => "CO2", "is_correct" => false],
                    ["question_id" => 7, "option" => "H2O", "is_correct" => true],
                    ["question_id" => 7, "option" => "CH4", "is_correct" => false],
                    ["question_id" => 8, "option" => "Oxygen", "is_correct" => false],
                    ["question_id" => 8, "option" => "Carbon dioxide", "is_correct" => false],
                    ["question_id" => 8, "option" => "Hydrogen", "is_correct" => true],
                    ["question_id" => 8, "option" => "Nitrogen", "is_correct" => false],
                    ["question_id" => 9, "option" => "Nitrogen", "is_correct" => true],
                    ["question_id" => 9, "option" => "Oxygen", "is_correct" => false],
                    ["question_id" => 9, "option" => "Carbon dioxide", "is_correct" => false],
                    ["question_id" => 9, "option" => "Water vapor", "is_correct" => false],
                    ["question_id" => 10, "option" => "Calcium", "is_correct" => true],
                    ["question_id" => 10, "option" => "Iron", "is_correct" => false],
                    ["question_id" => 10, "option" => "Sodium", "is_correct" => false],
                    ["question_id" => 10, "option" => "Zinc", "is_correct" => false],

                    ["question_id" => 11, "option" => "Radio Waves", "is_correct" => false],
                    ["question_id" => 11, "option" => "Microwaves", "is_correct" => false],
                    ["question_id" => 11, "option" => "Infrared Radiation", "is_correct" => false],
                    ["question_id" => 11, "option" => "Gamma Rays", "is_correct" => true],

                    ["question_id" => 12, "option" => "5 kg m/s", "is_correct" => false],
                    ["question_id" => 12, "option" => "8 kg m/s", "is_correct" => false],
                    ["question_id" => 12, "option" => "15 kg m/s", "is_correct" => false],
                    ["question_id" => 12, "option" => "15 kg m/s^2", "is_correct" => true],

                    ["question_id" => 13, "option" => "The velocity at which an object starts falling", "is_correct" => false],
                    ["question_id" => 13, "option" => "The maximum velocity that an object can attain while falling", "is_correct" => true],
                    ["question_id" => 13, "option" => "The acceleration due to gravity", "is_correct" => false],
                    ["question_id" => 13, "option" => "The velocity at which an object stops falling", "is_correct" => false],

                    ["question_id" => 14, "option" => "Frequency is inversely proportional to wavelength, and both are directly proportional to the speed of a wave", "is_correct" => true],
                    ["question_id" => 14, "option" => "Frequency is directly proportional to wavelength and inversely proportional to the speed of a wave", "is_correct" => false],
                    ["question_id" => 14, "option" => "Wavelength is inversely proportional to frequency, and both are directly proportional to the speed of a wave", "is_correct" => false],
                    ["question_id" => 14, "option" => "Wavelength, frequency, and the speed of a wave are unrelated", "is_correct" => false],

                    ["question_id" => 15, "option" => "Law of Conservation of Energy", "is_correct" => true],
                    ["question_id" => 15, "option" => "Law of Thermodynamic Equilibrium", "is_correct" => false],
                    ["question_id" => 15, "option" => "Law of Heat Transfer", "is_correct" => false],
                    ["question_id" => 15, "option" => "Law of Entropy", "is_correct" => false],

                    ["question_id" => 16, "option" => "Vaporization", "is_correct" => false],
                    ["question_id" => 16, "option" => "Condensation", "is_correct" => false],
                    ["question_id" => 16, "option" => "Freezing", "is_correct" => true],
                    ["question_id" => 16, "option" => "Sublimation", "is_correct" => false],

                    ["question_id" => 17, "option" => "Oxygen", "is_correct" => false],
                    ["question_id" => 17, "option" => "Hydrogen", "is_correct" => false],
                    ["question_id" => 17, "option" => "Carbon dioxide", "is_correct" => false],
                    ["question_id" => 17, "option" => "Hydrogen Sulfide", "is_correct" => true],

                    ["question_id" => 18, "option" => "Catalyst", "is_correct" => true],
                    ["question_id" => 18, "option" => "Reactant", "is_correct" => false],
                    ["question_id" => 18, "option" => "Product", "is_correct" => false],
                    ["question_id" => 18, "option" => "Inhibitor", "is_correct" => false],

                    ["question_id" => 19, "option" => "Sodium", "is_correct" => true],
                    ["question_id" => 19, "option" => "Copper", "is_correct" => false],
                    ["question_id" => 19, "option" => "Lithium", "is_correct" => false],
                    ["question_id" => 19, "option" => "Barium", "is_correct" => false],

                    ["question_id" => 20, "option" => "So", "is_correct" => false],
                    ["question_id" => 20, "option" => "Sd", "is_correct" => false],
                    ["question_id" => 20, "option" => "Na", "is_correct" => true],
                    ["question_id" => 20, "option" => "Ns", "is_correct" => false]
                ]
            );
        }
    }
}
